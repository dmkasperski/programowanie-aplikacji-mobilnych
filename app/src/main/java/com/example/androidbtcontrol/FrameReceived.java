package com.example.androidbtcontrol;

public interface FrameReceived {
	void onReceive(String[] data, String accelerometerAlert);
	void onWrongFrame();
}
