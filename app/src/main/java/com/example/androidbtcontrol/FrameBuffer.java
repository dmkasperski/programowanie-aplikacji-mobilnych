package com.example.androidbtcontrol;


public class FrameBuffer {
    private String buffer;
    private Frame frame;

    public FrameBuffer(FrameReceived frameReceived) {
        buffer="";
        frame=new Frame(frameReceived);
    }

    public void addToBuffer(String add) {
        buffer+=add;
    }

    public void checkFrame() {
        frame.readFrame(buffer);
        if(frame.loadData()) {
            clearBuffer();
        }
    }

    public void clearBuffer() {
        buffer="";
    }

    public String getBuffer() {
        return buffer;
    }
}
