package com.example.androidbtcontrol;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Frame {
	private static final char BEGIN_SIGN='a';
	private static final char END_SIGN='b';
	private static final char SEPARATOR='-';
	private static final String ACC_ALERT_SIGN="r";
	
	private FrameReceived frameReceived;
	private String frame;
	
	public Frame(FrameReceived frameReceived) {
		super();
		this.frameReceived = frameReceived;
	}

	public void readFrame(String frame) {
		this.frame = frame;
	}
	
	public void readCharArray(char[] array) {
		StringBuilder sb=new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			sb.append(array[i]);
		}
		frame=sb.toString();
		loadData();
	}
	
	public boolean loadData() {
		
		Pattern framePattern=Pattern.compile(""+BEGIN_SIGN+".+"+END_SIGN);
		Matcher frameMatcher=framePattern.matcher(frame);
		if(frameMatcher.find()) {
			String correctFrame=frameMatcher.group();
			ArrayList<String> dataToLoad=new ArrayList<>();
			String accelerometerAlert=null;
			System.out.println(correctFrame);
			Pattern valuePattern=Pattern.compile("[^"+BEGIN_SIGN+END_SIGN+SEPARATOR+"]+");
			Matcher valueMatcher=valuePattern.matcher(correctFrame);
			while (valueMatcher.find()) {
				String value=valueMatcher.group();
				if(!value.equals(ACC_ALERT_SIGN)) {
					dataToLoad.add(value);
				} else {
					accelerometerAlert=value;
				}
			}
			frameReceived.onReceive(dataToLoad.toArray(new String[0]), accelerometerAlert);
			return true;
		} else {
			frameReceived.onWrongFrame();
			return false;
		}
	}

}
