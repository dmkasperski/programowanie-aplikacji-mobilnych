package com.example.androidbtcontrol;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;
import java.util.UUID;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedReader;



import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.jjoe64.graphview.DefaultLabelFormatter;

import android.os.Handler;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity  implements SensorEventListener  {


    /*
    variables
     */
    private static final int btEnable = 1;

    BluetoothAdapter btAdapter;

    ArrayList<BluetoothDevice> pairedDevicesArrayList;

    TextView textInfo, control, gyro;
    TextView receivedData;
    TextView TempInfo,LightInfo,HumidityInfo;
    ListView listViewPairedDevice;

    RelativeLayout ManualControlInputPane;

    LinearLayout MainInputPane, AccelerometerInputPane;
    LinearLayout OptionsInputPane,TempretureInputPane;
    LinearLayout LightInputPane;
    LinearLayout HumidityInputPane;

    ImageButton btnForward;
    ImageButton btnBack;
    ImageButton btnLeft;
    ImageButton btnRight;

    ToggleButton servisMode;
    ToggleButton tiltAlert;
    Button btnDeleteTemp;
    Button btnDeleteLight;
    Button btnDeleteHumidity;


    String bForward = "";
    String bBack = "";
    String bLeft = "";
    String bRight = "";
    String bStop = "xs000y";
    String frame = "";

    String startBit = "x";
    String stopBit = "y";

    boolean prawda = false;


    GraphView graphTemp;
    GraphView graphLight;
    GraphView graphHumidity;

    //Files
    final File file = new File(Environment.getExternalStorageDirectory(), "Dane pomiarowe");
    final File filepathTemp = new File(file, "Temperatura.txt");
    final File filepathLight = new File(file, "Natężenie światła.txt");
    final File filepathHumidity = new File(file, "Wilgotność powietrza.txt");

    //Handler
    Handler btRD;
    final int handlerState = 0;

FrameBuffer frameBuffer;

    //Navigation Drawer
    ArrayAdapter<String> mPlanetTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    ArrayAdapter<BluetoothDevice> pairedDeviceAdapter; //Array of BTdevices
    private UUID myUUID;
    private final String KNOWN_UUID =
            "00001101-0000-1000-8000-00805F9B34FB";

    ConnectWithBTdevice myBTConnectBTdevice;
    ThreadCommunication myDeviceIsConnected;//devices are connected, sending messages is enable
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    //Sensor - accelerometer
    Sensor sensor;
    SensorManager sManager;

    /*
    Creating app
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        layout
         */
        setContentView(R.layout.activity_main);

        textInfo = (TextView) findViewById(R.id.textInfo);
        control = (TextView) findViewById(R.id.control);
        gyro = (TextView) findViewById(R.id.gyro);
        receivedData = (TextView) findViewById(R.id.receivedData);
        listViewPairedDevice = (ListView) findViewById(R.id.listViewPairedDevice);
        ManualControlInputPane = (RelativeLayout) findViewById(R.id.ManualControlInputPane);
        MainInputPane = (LinearLayout) findViewById(R.id.MainInputPane);
        AccelerometerInputPane = (LinearLayout) findViewById(R.id.AccelerometerInputPane);
        OptionsInputPane=(LinearLayout) findViewById(R.id.OptionsInputPane);
        TempretureInputPane=(LinearLayout) findViewById(R.id.TempretureInputPane);
        HumidityInputPane=(LinearLayout)findViewById(R.id.HumidityInputPane);
        LightInputPane=(LinearLayout) findViewById(R.id.LightInputPane);
        btnForward=(ImageButton) findViewById(R.id.btnForward);
        btnForward.setImageResource(R.drawable.buttonf);
        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnBack.setImageResource(R.drawable.buttonb);
        btnLeft = (ImageButton) findViewById(R.id.btnLeft);
        btnLeft.setImageResource(R.drawable.buttonl);
        btnRight = (ImageButton) findViewById(R.id.btnRight);
        btnRight.setImageResource(R.drawable.buttonr);

        servisMode =(ToggleButton) findViewById(R.id.servisMode);
        tiltAlert= (ToggleButton) findViewById(R.id.tiltAlert);
        btnDeleteTemp=(Button) findViewById(R.id.btnDeleteTemp);
        btnDeleteLight=(Button) findViewById(R.id.btnDeleteLight);
        btnDeleteHumidity = (Button) findViewById(R.id.btnDeleteHumidity);

        TempInfo=(TextView) findViewById(R.id.TempInfo);
        LightInfo=(TextView) findViewById(R.id.LightInfo);
        HumidityInfo=(TextView) findViewById(R.id.HumidityInfo);

        sManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        //Navigation Drawer

        mDrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mDrawerList = (ListView) findViewById(R.id.LeftDrawer);


        mPlanetTitles = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        mPlanetTitles.add("Connect");
        mPlanetTitles.add("Buttons");
        mPlanetTitles.add("Accelerometer");
        mPlanetTitles.add("Temperature");
        mPlanetTitles.add("Illuminance");
        mPlanetTitles.add("Humidity");
        mPlanetTitles.add("Options");

        mDrawerList.setAdapter(mPlanetTitles);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

       graphTemp=(GraphView) findViewById(R.id.graphTemp);
        graphLight=(GraphView) findViewById(R.id.graphLight);
        graphHumidity=(GraphView) findViewById(R.id.graphHumidity);
        graphTemp.getGridLabelRenderer().setLabelFormatter(new TempretureLabelFormatter(this.getApplicationContext()));
        graphLight.getGridLabelRenderer().setLabelFormatter(new LightLabelFormatter(this.getApplicationContext()));
        graphHumidity.getGridLabelRenderer().setLabelFormatter(new HumidityLabelFormatter(this.getApplicationContext()));

        //received data
frameBuffer = new FrameBuffer(new FrameReceived() {
    @Override
    public void onReceive(String[] data, String accelerometerAlert) {

        if(accelerometerAlert!=null&&tiltAlert.isChecked()) {
            Log.d("alert", accelerometerAlert);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ManualControlInputPane.setBackgroundColor(Color.RED);
                    AccelerometerInputPane.setBackgroundColor(Color.RED);
                }
            });


        }
        else{
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ManualControlInputPane.setBackgroundColor(Color.WHITE);
                    AccelerometerInputPane.setBackgroundColor(Color.WHITE);
                }
            });
        }
        Log.d("temp",data[0]);
        Log.d("light",data[1]);
        Log.d("hum",data[2]);
        if (!file.mkdirs()) {
            file.mkdirs();}
        try {
            FileWriter writerTemp = new FileWriter(filepathTemp, true);
            writerTemp.append(String.valueOf(System.currentTimeMillis())).append("-").append(data[0]).append("\n");
            writerTemp.flush();
            writerTemp.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileWriter writerLight = new FileWriter(filepathLight, true);
            writerLight.append(String.valueOf(System.currentTimeMillis())).append("-").append(data[1]).append("\n");
            writerLight.flush();
            writerLight.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileWriter writerHumidity = new FileWriter(filepathHumidity, true);
            writerHumidity.append(String.valueOf(System.currentTimeMillis())).append("-").append(data[2]).append("\n");
            writerHumidity.flush();
            writerHumidity.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWrongFrame() {

    }
});

        btnForward.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (myDeviceIsConnected != null) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        bForward="xf099y";
                        myDeviceIsConnected.SendBytes(bForward.getBytes());
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        myDeviceIsConnected.SendBytes(bStop.getBytes());
                    }

                }

                return true;
            }
        });
        btnBack.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (myDeviceIsConnected != null) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        bBack="xb099y";
                        myDeviceIsConnected.SendBytes(bBack.getBytes());
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        myDeviceIsConnected.SendBytes(bStop.getBytes());
                    }

                }
                return true;
            }
        });
       btnLeft.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (myDeviceIsConnected != null) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        bLeft="xl099y";
                        myDeviceIsConnected.SendBytes(bLeft.getBytes());
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        myDeviceIsConnected.SendBytes(bStop.getBytes());
                    }

                }
                return true;
            }
        });

        btnRight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (myDeviceIsConnected != null) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        bRight = "xr099y";
                        myDeviceIsConnected.SendBytes(bRight.getBytes());
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        myDeviceIsConnected.SendBytes(bStop.getBytes());
                    }

                }
                return true;
            }
        });

        //Deleting Files
        btnDeleteTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filepathTemp.exists()) {
                    filepathTemp.delete();
                }}});
        btnDeleteLight.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (filepathLight.exists()){
                    filepathLight.delete();}
            }});
        btnDeleteHumidity.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if (filepathHumidity.exists()){
                    filepathHumidity.delete();}
            }});



/*
The device is capable for communicating with other devices via Bluetooth other toast
 */
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            Toast.makeText(this, "BLUETOOTH IS NOT SUPPORTED", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        //using the known SPP UUID
        myUUID = UUID.fromString(KNOWN_UUID);
/*
Assing default BT adapter to variable, if device does not have BT toast
 */
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) {
            Toast.makeText(this,
                    "Bluetooth is not available",
                    Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        String stInfo = btAdapter.getName() + "\n" + btAdapter.getAddress();//my device
        textInfo.setText("Select device");

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    public String frameToSendAcc(int value, String direction) {
        if (value < 10)
            frame = startBit + direction + "00" + value + stopBit;
        else if (value > 99)
            frame = startBit + direction + value + stopBit;
        else
            frame = startBit + direction + "0" + value + stopBit;
        return frame;
    }


    /*
    Called after onCreate.
     */
    @Override
    protected void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();

        /*
        This system activity will return once Bluetooth has completed turning on, or the user has decided not to turn Bluetooth on.

        enableIntent - intent to start
        btEnable=1 so this code will be return in onActivityResult() when activity exits
         */
        if (!btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, btEnable);
        }

        setup();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.androidbtcontrol/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    /*
    Generating a listView of paired devices on start of aplication
     */
    private void setup() {

        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            pairedDevicesArrayList = new ArrayList<BluetoothDevice>();

            for (BluetoothDevice device : pairedDevices) {
                pairedDevicesArrayList.add(device);
            }


            pairedDeviceAdapter = new ArrayAdapter<BluetoothDevice>(this,
                    android.R.layout.simple_list_item_1, pairedDevicesArrayList);
            listViewPairedDevice.setAdapter(pairedDeviceAdapter);

            /*
            Clicking on item on the list
            Choosing a device, details about device and connection, conneting with it
            AdapterView: The AdapterView where the click happened.
            View: The view within the AdapterView that was clicked (this will be a view provided by the adapter)
            int: The position of the view in the adapter.
            long: The row id of the item that was clicked.
             */

            listViewPairedDevice.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    BluetoothDevice device = (BluetoothDevice) parent.getItemAtPosition(position);
                    Toast.makeText(MainActivity.this,
                            "Name: " + device.getName() + "\n"
                                    + "Address: " + device.getAddress() + "\n"
                                    + "BondState: " + device.getBondState() + "\n"
                                    + "BluetoothClass: " + device.getBluetoothClass() + "\n"
                                    + "Class: " + device.getClass(),
                            Toast.LENGTH_LONG).show();

                    textInfo.setText("Wait, connecting...");

                    myBTConnectBTdevice = new ConnectWithBTdevice(device);//device get from listView is assing to btDevice from this class
                    myBTConnectBTdevice.start();//starting this method
                }
            });
        }

    }


    @Override
    /*
    Perform any final cleanup before an activity is destroyed.
    Make sure socket is canceled.
     */
    protected void onDestroy() {
        super.onDestroy();

        if (myBTConnectBTdevice != null) {
            myBTConnectBTdevice.cancel();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == btEnable) {
            if (resultCode == Activity.RESULT_OK) {
                setup();
            } else {
                Toast.makeText(this,
                        "BlueTooth NOT enabled",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    /*
        Called in ThreadConnectBTdevice once connect successed
        to start ThreadConnected
        starts socket connect
         */
    private void startThreadConnected(BluetoothSocket socket) {

        myDeviceIsConnected = new ThreadCommunication(socket);
        myDeviceIsConnected.start();
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.androidbtcontrol/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int accX = Math.round(event.values[0] * 10);
        int accY = Math.round(event.values[1] * 10);
        int accZ = Math.round(event.values[2] * 10);
        if (accX > 99) {
            accX = 99;
        }
        if (accY > 99) {
            accY = 99;
        }
        if (accZ > 99) {
            accZ = 99;
        }
        if (accX < -99) {
            accX = -99;
        }
        if (accY < -99) {
            accY = -99;
        }
        if (accZ < -99) {
            accZ = -99;
        }

        if ((accZ == 35 || accZ == 40 || accZ == 45 || accZ == 50 || accZ == 55 || accZ == 60 || accZ == 65 || accZ == 70 || accZ == 75 || accZ == 80 || accZ == 85 || accZ == 90 || accZ == 95 || accZ == 99) && prawda && accX < 30 && accX > -30) {

            myDeviceIsConnected.SendBytes(frameToSendAcc(accZ, "f").getBytes());
            try {
               Thread.sleep(50);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        if ((accZ == -15 || accZ == -10 || accZ == -5 || accZ == 0 || accZ == 5 || accZ == 10 || accZ == 15 || accZ == 20 || accZ == 25 || accZ == 30) && prawda && accX < 30 && accX > -30) {
            myDeviceIsConnected.SendBytes(bStop.getBytes());
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        if ((accX == -20 || accX == -25 || accX == -30 || accX == -35 || accZ == -40 || accZ == -45 || accZ == -50 || accZ == -55 || accZ == -60 || accZ == -65 || accZ == -70 || accZ == -75 || accZ == -80 || accZ == -85 || accZ == -90 || accZ == -95 || accZ == -99) && prawda && accX < 30 && accX > -30) {
            myDeviceIsConnected.SendBytes(frameToSendAcc(-accZ, "b").getBytes());
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        if ((accX == -30 || accX == -35 || accX == -40 || accX == -45 || accX == -50 || accX == -55 || accX == -60 || accX == -65 || accX == -70 || accX == -75 || accX == -80 || accX == -85 || accX == -90 || accX == -95 || accX == -99) && prawda) {
            myDeviceIsConnected.SendBytes(frameToSendAcc(-accX, "r").getBytes());
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        if ((accX == 30 || accX == 35 || accX == 40 || accX == 45 || accX == 50 || accX == 55 || accX == 60 || accX == 65 || accX == 70 || accX == 75 || accX == 80 || accX == 85 || accX == 90 || accX == 95 || accX == 99) && prawda) {
            myDeviceIsConnected.SendBytes(frameToSendAcc(accX, "l").getBytes());
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /*
    Extending class Thread
    ConnextWithBTdevice:



    Background Thread to handle BlueTooth connecting
    */
    private class ConnectWithBTdevice extends Thread {

        private BluetoothSocket btSocket = null;//declaration of empty btSocket
        private final BluetoothDevice btDevice;//declaration of final btDevice

        /*
        ThreadConnectBTdevice
        method that's trying to connect choosen device with existing socket
         */
        private ConnectWithBTdevice(BluetoothDevice device) {
            btDevice = device;

            try {
                    btSocket = device.createRfcommSocketToServiceRecord(myUUID);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        /*
            Connecting 2 devices
            if success returns btSocket with 2 connected devices
         */
        public void run() {
            boolean success = false;
            btAdapter.cancelDiscovery();
            try {
                btSocket.connect();//Attempt to connect to a remote device.
                success = true;
            } catch (IOException e) {
                //failed connecting to remote device so close() btSocket
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textInfo.setText("Connection failed, please select device again");
                    }
                });
                try {
                    btSocket.close();
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }

            if (success) {
                //connect successful
                final String msgconnected = "Connected\n"
                        + "BluetoothDevice: " + btDevice;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textInfo.setText(msgconnected);
                        MainInputPane.setVisibility(View.GONE);
                        AccelerometerInputPane.setVisibility(View.GONE);
                        ManualControlInputPane.setVisibility(View.VISIBLE);
                    }
                });

                startThreadConnected(btSocket);//socket where 2 devices are connected
            } else {
                //fail
            }
        }

        /*
        cancel()
        method that's closing socket
         */
        public void cancel() {

            Toast.makeText(getApplicationContext(),
                    "BluetoothSocket closed",
                    Toast.LENGTH_LONG).show();

            try {
                btSocket.close();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }

    /*
    Extends class Thread
    ThreadCommunication:
    Background Thread to handle Bluetooth data communication
    after connected


     */
    private class ThreadCommunication extends Thread {
        private final BluetoothSocket connectedBtSocket;
        private final InputStream connectedInputStream;
        private final OutputStream connectedOutputStream;

        public ThreadCommunication(BluetoothSocket socket) {
            connectedBtSocket = socket;


            InputStream in = null;              //empty stream
            OutputStream out = null;            //empty stream
/*
Get the input and output streams associated with this socket.
The streams will be returned even if the socket is not yet connected,
but operations on that stream will throw IOException until the associated socket is connected.
 */
            try {
                in = socket.getInputStream();
                out = socket.getOutputStream();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            connectedInputStream = in;
            connectedOutputStream = out;
        }

        //TODO
        @Override
        public void run() {
            byte[] buffer = new byte[256];
            int bytes;
           while (true) {
               try {
                   bytes = connectedInputStream.read(buffer);
                   String strReceived = new String(buffer, 0, bytes);
                   frameBuffer.addToBuffer(strReceived);
                   frameBuffer.checkFrame();
               }
                       catch(IOException e){
                           e.printStackTrace();
                       }
           }}


        /*
        sending message buffer
        */
        public void SendBytes(byte[] buffer) {
            try {
                connectedOutputStream.write(buffer);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public void cancel() {
            try {
                connectedBtSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


    }





    /*nav drawer*/
    public class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            mDrawerLayout.closeDrawer(mDrawerList);

           if (Integer.toString(position) == "1"&& (myDeviceIsConnected != null||servisMode.isChecked()) ) {
                MainInputPane.setVisibility(View.GONE);
                LightInputPane.setVisibility(View.GONE);
                AccelerometerInputPane.setVisibility(View.GONE);
                ManualControlInputPane.setVisibility(View.VISIBLE);
                OptionsInputPane.setVisibility(View.GONE);
                TempretureInputPane.setVisibility(View.GONE);
                HumidityInputPane.setVisibility(View.GONE);
                prawda = false;
            }
               else{
                      textInfo.setText("There is no connection");}
            if (Integer.toString(position) == "2" && (myDeviceIsConnected != null||servisMode.isChecked())) {
                ManualControlInputPane.setVisibility(View.GONE);
                MainInputPane.setVisibility(View.GONE);
                LightInputPane.setVisibility(View.GONE);
                TempretureInputPane.setVisibility(View.GONE);
                OptionsInputPane.setVisibility(View.GONE);
                AccelerometerInputPane.setVisibility(View.VISIBLE);
                HumidityInputPane.setVisibility(View.GONE);
                prawda = true;
            }               else{
                textInfo.setText("There is no connection");}
            if (Integer.toString(position) == "0") {
                ManualControlInputPane.setVisibility(View.GONE);
                MainInputPane.setVisibility(View.VISIBLE);
                OptionsInputPane.setVisibility(View.GONE);
                LightInputPane.setVisibility(View.GONE);
                AccelerometerInputPane.setVisibility(View.GONE);
                TempretureInputPane.setVisibility(View.GONE);
                HumidityInputPane.setVisibility(View.GONE);
                prawda = false;
            }
            if(Integer.toString(position)=="6"){
                OptionsInputPane.setVisibility(View.VISIBLE);
                LightInputPane.setVisibility(View.GONE);
                AccelerometerInputPane.setVisibility(View.GONE);
                ManualControlInputPane.setVisibility(View.GONE);
                MainInputPane.setVisibility(View.GONE);
                TempretureInputPane.setVisibility(View.GONE);
                HumidityInputPane.setVisibility(View.GONE);
                prawda=false;
            }
            if(Integer.toString(position)=="3"){
                TempretureInputPane.setVisibility(View.VISIBLE);
                OptionsInputPane.setVisibility(View.GONE);
                LightInputPane.setVisibility(View.GONE);
                AccelerometerInputPane.setVisibility(View.GONE);
                ManualControlInputPane.setVisibility(View.GONE);
                MainInputPane.setVisibility(View.GONE);
                HumidityInputPane.setVisibility(View.GONE);

                prawda=false;
                double x=0.0;
                double y=0.0;

                double maxV=0.0;
                double minV=50.0;
                double avV=0.0;
                int numberOfLines=0;
                boolean first=true;
                graphTemp.removeAllSeries();
                try {
                    BufferedReader in
                            = new BufferedReader(new FileReader(filepathTemp));
                    String line;
                    while((line = in.readLine()) != null)
                    {

                        if (line.contains("-")) {
                            int p = line.indexOf('-');
                            if (p >= 0) {
                                final String left = line.substring(0, p);
                                final String right = line.substring(p + 1);
                                x=Double.parseDouble(left);
                                y=Double.parseDouble(right);
                            }}


                        PointsGraphSeries<DataPoint> series = new PointsGraphSeries<>(new DataPoint[]{
                                new DataPoint(x,y),


                        });

                        graphTemp.addSeries(series);
                        double TempMaxV=series.getHighestValueY();
                        double TempMinV=series.getLowestValueY();
                        if(TempMaxV>maxV){
                            maxV=series.getHighestValueY();}
                        if(TempMinV<minV){
                            minV=series.getLowestValueY();}
                        avV=avV+TempMaxV;
                        numberOfLines++;
                        series.setShape(PointsGraphSeries.Shape.POINT);
                        series.setSize(5);
                        series.setColor(Color.BLUE);

                        graphTemp.getViewport().setMinY(0);
                        graphTemp.getViewport().setMaxY(50);
                        graphTemp.getViewport().setMaxX(x);
                        if(first){
                            graphTemp.getViewport().setMinX(x);
                            first=false;
                        }
                        graphTemp.getViewport().setXAxisBoundsManual(true);
                        graphTemp.getViewport().setYAxisBoundsManual(true);
                        graphTemp.getGridLabelRenderer().setHorizontalLabelsAngle(140);
                    }
                    in.close();
                    if(numberOfLines>1) {
                        TempInfo.setText("Temperature:\nLowest Value: " + minV + "\u00b0C" + "\n" + "Average Value: " + (double)Math.round(avV / numberOfLines *10d )/10d + "\u00b0C" + "\n" + "Highest Value: " + maxV + "\u00b0C");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(maxV==0){
                    TempInfo.setText("There is no Temperature data  in phone memory");
                }
            }
            if(Integer.toString(position)=="4"){
                LightInputPane.setVisibility(View.VISIBLE);
                TempretureInputPane.setVisibility(View.GONE);
                OptionsInputPane.setVisibility(View.GONE);
                AccelerometerInputPane.setVisibility(View.GONE);
                ManualControlInputPane.setVisibility(View.GONE);
                MainInputPane.setVisibility(View.GONE);
                HumidityInputPane.setVisibility(View.GONE);

                prawda=false;
                double x=0.0;
                double y=0.0;

                double maxV=0.0;
                double minV=100.0;
                double avV=0.0;
                int numberOfLines=0;
                boolean first=true;
                graphLight.removeAllSeries();
                try {
                    BufferedReader in
                            = new BufferedReader(new FileReader(filepathLight));
                    String line;
                    while((line = in.readLine()) != null)
                    {

                        if (line.contains("-")) {
                            int p = line.indexOf('-');
                            if (p >= 0) {
                                final String left = line.substring(0, p);
                                final String right = line.substring(p + 1);
                                x=Double.parseDouble(left);
                                y=Double.parseDouble(right);
                            }}

                        PointsGraphSeries<DataPoint> series = new PointsGraphSeries<>(new DataPoint[]{
                                new DataPoint(x,y),


                        });
                        graphLight.addSeries(series);
                        double TempMaxV=series.getHighestValueY();
                        double TempMinV=series.getLowestValueY();
                        if (TempMaxV > maxV) {
                            maxV=series.getHighestValueY();}
                        if(TempMinV< minV) {
                            minV=series.getLowestValueY();}
                        avV=avV+TempMaxV;
                        numberOfLines++;
                        series.setShape(PointsGraphSeries.Shape.POINT);
                        series.setSize(5);
                        series.setColor(Color.BLUE);
                        graphLight.getViewport().setMinY(0);
                        graphLight.getViewport().setMaxY(1200);

                        graphLight.getViewport().setYAxisBoundsManual(true);
                        graphLight.getViewport().setMaxX(x);
                        if(first){
                            graphLight.getViewport().setMinX(x);
                            first=false;
                        }
                        graphLight.getViewport().setXAxisBoundsManual(true);
                        graphLight.getGridLabelRenderer().setHorizontalLabelsAngle(140);
                    }
                    in.close();
                    if(numberOfLines>1) {
                        LightInfo.setText("Illuminance:\nLowest Value: " + minV + "lx" + "\n" + "Average Value: " + (double)Math.round(avV / numberOfLines *10d )/10d  + "lx" + "\n" + "Highest Value: " + maxV + "lx");
                    }if(maxV==0){
                        LightInfo.setText("There is no Illuminance data in phone memory");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(Integer.toString(position)=="5"){
                HumidityInputPane.setVisibility(View.VISIBLE);
                LightInputPane.setVisibility(View.GONE);
                TempretureInputPane.setVisibility(View.GONE);
                OptionsInputPane.setVisibility(View.GONE);
                AccelerometerInputPane.setVisibility(View.GONE);
                ManualControlInputPane.setVisibility(View.GONE);
                MainInputPane.setVisibility(View.GONE);

                prawda=false;
                double x=0.0;
                double y=0.0;
                double maxV=0.0;
                double minV=100.0;
                double avV=0.0;
                int numberOfLines=0;
                boolean first = true;
                graphHumidity.removeAllSeries();
                try {
                    BufferedReader in
                            = new BufferedReader(new FileReader(filepathHumidity));
                    String line;
                    while((line = in.readLine()) != null)
                    {

                        if (line.contains("-")) {
                            int p = line.indexOf('-');
                            if (p >= 0) {
                                final String left = line.substring(0, p);
                                final String right = line.substring(p + 1);
                                x=Double.parseDouble(left);
                                y=Double.parseDouble(right);
                            }}

                        PointsGraphSeries<DataPoint> series = new PointsGraphSeries<>(new DataPoint[]{
                                new DataPoint(x,y),


                        });
                        graphHumidity.addSeries(series);
                        double TempMaxV=series.getHighestValueY();
                        double TempMinV=series.getLowestValueY();
                        if(TempMaxV>maxV){
                        maxV=series.getHighestValueY();}
                        if(TempMinV<minV){
                        minV=series.getLowestValueY();}
                        avV=avV+TempMaxV;
                        numberOfLines++;
                        series.setShape(PointsGraphSeries.Shape.POINT);
                        series.setSize(5);

                        series.setColor(Color.BLUE);
                        graphHumidity.getViewport().setMaxY(100);
                        graphHumidity.getViewport().setMinY(0);

                        graphHumidity.getViewport().setYAxisBoundsManual(true);
                        graphHumidity.getViewport().setMaxX(x);
                        if(first){
                            graphHumidity.getViewport().setMinX(x);
                            first=false;
                        }
                        graphHumidity.getViewport().setXAxisBoundsManual(true);
                        graphHumidity.getGridLabelRenderer().setHorizontalLabelsAngle(140);
                    }

                    in.close();
                    if(numberOfLines>0) {
                        HumidityInfo.setText("Humidity:\nLowest Value: " + minV + "%" + "\n" + "Average Value: " + (double)Math.round(avV / numberOfLines *10d )/10d  + "%" + "\n" + "Highest Value: " + maxV + "%");
                    }
                    if(maxV==0){
                    HumidityInfo.setText("There is no Humidity data in phone memory");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public class TempretureLabelFormatter extends DefaultLabelFormatter{
        /**
         * the date format that will convert
         * the unix timestamp to string
         */
        protected final java.text.DateFormat mDateFormat;

        /**
         * calendar to avoid creating new date objects
         */
        protected final Calendar mCalendar;

        /**
         * create the formatter with the Android default date format to convert
         * the x-values.
         *
         * @param context the application context
         */
        public TempretureLabelFormatter(Context context) {

            mDateFormat=android.text.format.DateFormat.getTimeFormat(context);
            mCalendar = Calendar.getInstance();
        }

        /**
         * create the formatter with your own custom
         * date format to convert the x-values.
         *
         * @param context the application context
         * @param dateFormat custom date format
         */
        public TempretureLabelFormatter(Context context, java.text.DateFormat dateFormat) {
            mDateFormat = dateFormat;
            mCalendar = Calendar.getInstance();
        }

        /**
         * formats the x-values as date string.
         *
         * @param value raw value
         * @param isValueX true if it's a x value, otherwise false
         * @return value converted to string
         */
        @Override
        public String formatLabel(double value, boolean isValueX) {
            if (isValueX) {
                // format as date
                mCalendar.setTimeInMillis((long) value);
                return mDateFormat.format(mCalendar.getTimeInMillis());
            } else {
                return super.formatLabel(value, isValueX) + "\u00b0C";
            }
        }
    }


    public class LightLabelFormatter extends DefaultLabelFormatter{
        /**
         * the date format that will convert
         * the unix timestamp to string
         */
        protected final java.text.DateFormat mDateFormat;

        /**
         * calendar to avoid creating new date objects
         */
        protected final Calendar mCalendar;

        /**
         * create the formatter with the Android default date format to convert
         * the x-values.
         *
         * @param context the application context
         */
        public LightLabelFormatter(Context context) {

            mDateFormat=android.text.format.DateFormat.getTimeFormat(context);
            mCalendar = Calendar.getInstance();
        }

        /**
         * create the formatter with your own custom
         * date format to convert the x-values.
         *
         * @param context the application context
         * @param dateFormat custom date format
         */
        public LightLabelFormatter(Context context, java.text.DateFormat dateFormat) {
            mDateFormat = dateFormat;
            mCalendar = Calendar.getInstance();
        }

        /**
         * formats the x-values as date string.
         *
         * @param value raw value
         * @param isValueX true if it's a x value, otherwise false
         * @return value converted to string
         */
        @Override
        public String formatLabel(double value, boolean isValueX) {
            if (isValueX) {
                // format as date
                mCalendar.setTimeInMillis((long) value);
                return mDateFormat.format(mCalendar.getTimeInMillis());
            } else {
                return super.formatLabel(value, isValueX) + "lx";
            }
        }
    }

    public class HumidityLabelFormatter extends DefaultLabelFormatter{
        /**
         * the date format that will convert
         * the unix timestamp to string
         */
        protected final java.text.DateFormat mDateFormat;

        /**
         * calendar to avoid creating new date objects
         */
        protected final Calendar mCalendar;

        /**
         * create the formatter with the Android default date format to convert
         * the x-values.
         *
         * @param context the application context
         */
        public HumidityLabelFormatter(Context context) {

            mDateFormat=android.text.format.DateFormat.getTimeFormat(context);
            mCalendar = Calendar.getInstance();
        }

        /**
         * create the formatter with your own custom
         * date format to convert the x-values.
         *
         * @param context the application context
         * @param dateFormat custom date format
         */
        public HumidityLabelFormatter(Context context, java.text.DateFormat dateFormat) {
            mDateFormat = dateFormat;
            mCalendar = Calendar.getInstance();
        }

        /**
         * formats the x-values as date string.
         *
         * @param value raw value
         * @param isValueX true if it's a x value, otherwise false
         * @return value converted to string
         */
        @Override
        public String formatLabel(double value, boolean isValueX) {
            if (isValueX) {
                // format as date
                mCalendar.setTimeInMillis((long) value);
                return mDateFormat.format(mCalendar.getTimeInMillis());
            } else {
                return super.formatLabel(value, isValueX) + "%";
            }
        }
    }
}